const express = require('express')
const next = require('next')
var sessionStorage = require('sessionstorage')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()


var mysql = require('mysql');
var db_config = {
  host: "localhost",
  user: "root",
  password: "arbitrabotmysql",
  database: 'arbitrabot'
};
var mysql_connect;
// var mysql_connect = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "",
//   database: 'arbitrabot'
// });

// mysql_connect.connect(function(err) {
//   if (err) throw err;
// });

function handleDisconnect() {
  mysql_connect = mysql.createConnection(db_config); // Recreate the connection, since
                                                  // the old one cannot be reused.
  mysql_connect.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  mysql_connect.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();

app.prepare()
  .then(() => {
    const server = express()
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

///////////////////////////////////////////////  PAGE ROUTE  //////////////////////
    server.get('/', (req, res) => {
      if (sessionStorage.getItem('username') && sessionStorage.getItem('password')) {
        console.log('YES')
        return app.render(req, res, '/', req.query)
      } else {
        console.log('NO')
        return res.redirect('/signin')
      }
    })

    server.get('/index', (req, res) => {
      return res.redirect('/')
    })

    server.get('/currency', (req, res) => {
      if (sessionStorage.getItem('username') && sessionStorage.getItem('password'))
        return app.render(req, res, '/currency', req.query)
      else
        return res.redirect('/signin')
    })

    server.get('/parity', (req, res) => {
      if (sessionStorage.getItem('username') && sessionStorage.getItem('password'))
        return app.render(req, res, '/parity', req.query)
      else
      return res.redirect('/signin')
    })

    server.get('/balance', (req, res) => {
      if (sessionStorage.getItem('username') && sessionStorage.getItem('password'))
        return app.render(req, res, '/balance', req.query)
      else
        return res.redirect('/signin')
    })

///////////////////////////////////////////////  CRUD  //////////////////////    

    server.get('/signin/:username/:password', (req, res) => {
      mysql_connect.query("SELECT * FROM users where username='" + req.params.username + "' and password='" + req.params.password + "'", (err, result, fields) => {
        if (err) throw err;
        if (result.length == 0)
          return res.redirect('/signin')
        else {
          sessionStorage.setItem('userid',   result[0].id)
          sessionStorage.setItem('username', result[0].username)
          sessionStorage.setItem('password', result[0].password)
          return res.redirect('/')
        }
      })
    })

    server.get('/accountkey/:exchange/:type/:key', (req, res) => {
      mysql_connect.query("SELECT * FROM accounts WHERE userid=" + sessionStorage.getItem('userid') + " and exchange=" + req.params.exchange, (err, result, fields) => {
        if (err) throw err;

        if (result.length == 0) {
          var apikey = '', secretkey = '';
          if (req.params.type == 1) apikey = req.params.key;
          if (req.params.type == 2) secretkey = req.params.key;
          mysql_connect.query("INSERT INTO accounts (userid, apikey, secretkey, exchange) VALUES (" + sessionStorage.getItem('userid') + ", '" + apikey + "', '" + secretkey + "', " + req.params.exchange + ")");
        } else {
          if (req.params.type == 1)
            var setfield = 'apikey';
          else
            var setfield = 'secretkey';
          mysql_connect.query("UPDATE accounts SET " + setfield + "= '" + req.params.key + "' WHERE id = " + result[0].id);
        }
      })
    })

    server.get('/coinkey/:exchange/:type/:key', (req, res) => {
      
    })

    server.get('/paritylimit/:exchange/:pair1/:pair2/:value/:type', (req, res) => {
      let exchanges = {
        'BINANCE' : 1,
        'BTCTURK' : 2,
        'KOINEKS' : 3
      }
      let parity = req.params.pair1 + '/' + req.params.pair2;
      mysql_connect.query("SELECT * FROM parities where exchange='" + exchanges[req.params.exchange] + "' and parity='" + parity + "'", (err, result, fields) => {
        if (err) throw err;
        if (result.length > 0) {
          let pid = result[0].id;
          mysql_connect.query("SELECT * FROM limits where userid=" + sessionStorage.getItem('userid') + " and pid=" + pid, (err1, result1, fields1) => {
            if (err1) throw err1;
            let blimit = 0;
            let rlimit = 0;
            if (result1.length == 0) {
              if (req.params.type == 0)
                blimit = req.params.value;
              else
                rlimit = req.params.value;
              mysql_connect.query("INSERT INTO limits (userid, pid, blimit, rlimit) VALUES (" + sessionStorage.getItem('userid') + "," + pid + "," + blimit + "," + rlimit + ")");
            } else {
              if (req.params.type == 0)
                var setfield = 'blimit';
              else
                var setfield = 'rlimit';
              mysql_connect.query("UPDATE limits SET " + setfield + "=" + req.params.value + " WHERE id=" + result1[0].id);
            }
          })
        }
      })
    })

    server.get('/getaccountkey', (req, res) => {
      mysql_connect.query("SELECT * FROM accounts WHERE userid=" + sessionStorage.getItem('userid'), (err, result, fields) => {
        if (err) throw err;

        var apikeys = {
          'BINANCE': '',
          'BTCTURK': '',
          'KOINEKS': ''
        }
        var secretkeys = {
          'BINANCE': '',
          'BTCTURK': '',
          'KOINEKS': ''
        }
        for (var record of result) {
          switch (record.exchange) {
            case 1:
              apikeys['BINANCE'] = record.apikey;
              secretkeys['BINANCE'] = record.secretkey;
              break;
            case 2:
              apikeys['BTCTURK'] = record.apikey;
              secretkeys['BTCTURK'] = record.secretkey;
              break;
            case 3:
              apikeys['KOINEKS'] = record.apikey;
              secretkeys['KOINEKS'] = record.secretkey;
              break;
          }
        }
        res.send({apikeys: apikeys, secretkeys: secretkeys})
      })
    })

    server.get('/getparitylimit', (req, res) => {
      let query = `
        SELECT pt.exchange, pt.parity, lm.blimit, lm.rlimit
        from limits lm
        left join parities pt on lm.pid = pt.id
        where lm.userid=` + sessionStorage.getItem('userid') + `
      `
      mysql_connect.query(query, (err, result, fields) => {
        if (err) throw err;
        res.send({paritylimits: result})
      })
    })
///////////////////////////////////////////////  FETCH 3rd PARTY API  //////////////////////    

    server.get('/binance-depth/:parity', (req, res) => {
      const binance = require('node-binance-api')().options({
        APIKEY: 'noneed',
        APISECRET: 'noneed',
        useServerTime: true,
        test: true
      });
      const parity = req.params.parity;
      binance.depth(parity, (error, depth, symbol) => {
        binance.aggTrades(parity, {limit:18}, (error, response)=>{
          res.send({ depth: depth, history: response })
        });          
      });
    })

    server.get('/balance-list', (req, res) => {
      var apikey = 'init';
      var secretkey = 'init';
      mysql_connect.query("SELECT * FROM accounts WHERE userid=" + sessionStorage.getItem('userid') + " and exchange=1", (err, result, fields) => {
        if (err) throw err;        
        if (result.length > 0) {
          apikey = result[0].apikey;
          secretkey = result[0].secretkey;
        }
        const binance = require('node-binance-api')().options({
          APIKEY: apikey,
          APISECRET: secretkey,
          useServerTime: false,
          test: true
        });
        binance.useServerTime(function() {
          binance.balance((error, balances) => {
            if (error)
              res.send({binance: {}})
            else {
              res.send({binance: balances})
              /*let T = { BTC: { available: '0.25206464', onOrder: '0.00177975' },
                      LTC: { available: '0.00000000', onOrder: '0.00000000' },
                      ETH: { available: '1.14109900', onOrder: '0.00000000' },
                      BNC: { available: '0.00000000', onOrder: '0.00000000' },
                      ICO: { available: '0.00000000', onOrder: '0.00000000' },
                      NEO: { available: '0.00000000', onOrder: '0.00000000' },
                      BNB: { available: '41.33761879', onOrder: '0.00000000' },
                      QTUM: { available: '0.00000000', onOrder: '0.00000000' },
                      EOS: { available: '0.00000000', onOrder: '0.00000000' },
                      SNT: { available: '0.00000000', onOrder: '0.00000000' },
                      BNT: { available: '0.00000000', onOrder: '0.00000000' },
                      GAS: { available: '0.00000000', onOrder: '0.00000000' },
                      BCC: { available: '0.00000000', onOrder: '0.00000000' },
                      BTM: { available: '0.00000000', onOrder: '0.00000000' },
                      USDT: { available: '5678.9', onOrder: '0.00000000' },
                      HCC: { available: '0.00000000', onOrder: '0.00000000' },
                      HSR: { available: '0.00000000', onOrder: '0.00000000' },
                      OAX: { available: '0.00000000', onOrder: '0.00000000' },
                      DNT: { available: '0.00000000', onOrder: '0.00000000' },
                      MCO: { available: '0.00000000', onOrder: '0.00000000' },
                      ICN: { available: '0.00000000', onOrder: '0.00000000' },
                      ELC: { available: '0.00000000', onOrder: '0.00000000' },
                      PAY: { available: '0.00000000', onOrder: '0.00000000' },
                      ZRX: { available: '0.00000000', onOrder: '0.00000000' },
                      OMG: { available: '0.00000000', onOrder: '0.00000000' },
                      WTC: { available: '0.00000000', onOrder: '0.00000000' },
                      LRX: { available: '0.00000000', onOrder: '0.00000000' },
                      YOYO: { available: '0.00000000', onOrder: '0.00000000' },
                      LRC: { available: '0.00000000', onOrder: '0.00000000' },
                      LLT: { available: '0.00000000', onOrder: '0.00000000' },
                      TRX: { available: '0.00000000', onOrder: '0.00000000' },
                      FID: { available: '0.00000000', onOrder: '0.00000000' },
                      SNGLS: { available: '0.00000000', onOrder: '0.00000000' },
                      STRAT: { available: '0.00000000', onOrder: '0.00000000' },
                      BQX: { available: '0.00000000', onOrder: '0.00000000' },
                      FUN: { available: '0.00000000', onOrder: '0.00000000' },
                      KNC: { available: '0.00000000', onOrder: '0.00000000' },
                      CDT: { available: '0.00000000', onOrder: '0.00000000' },
                      XVG: { available: '0.00000000', onOrder: '0.00000000' },
                      IOTA: { available: '0.00000000', onOrder: '0.00000000' },
                      SNM: { available: '0.76352833', onOrder: '0.00000000' },
                      LINK: { available: '0.00000000', onOrder: '0.00000000' },
                      CVC: { available: '0.00000000', onOrder: '0.00000000' },
                      TNT: { available: '0.00000000', onOrder: '0.00000000' },
                      REP: { available: '0.00000000', onOrder: '0.00000000' },
                      CTR: { available: '0.00000000', onOrder: '0.00000000' },
                      MDA: { available: '0.00000000', onOrder: '0.00000000' },
                      MTL: { available: '0.00000000', onOrder: '0.00000000' },
                      SALT: { available: '0.00000000', onOrder: '0.00000000' },
                      NULS: { available: '0.00000000', onOrder: '0.00000000' }
                  }
              res.send({binance: T})*/
            }
          })
        })
      })
    })

    server.get('/price-list', (req, res) => {
      const binance = require('node-binance-api')().options({
        APIKEY: 'noneed',
        APISECRET: 'noneed',
        useServerTime: true,
        test: true
      });
      binance.prices((error, ticker) => {
        res.send({ data: ticker })
      });
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(port, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })
  })