-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2018 at 06:13 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arbitrabot`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `apikey` varchar(300) NOT NULL,
  `secretkey` varchar(300) NOT NULL,
  `exchange` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `limits`
--

CREATE TABLE `limits` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `blimit` int(11) NOT NULL,
  `rlimit` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `limits`
--

INSERT INTO `limits` (`id`, `userid`, `pid`, `blimit`, `rlimit`) VALUES
(1, 1, 1, 100, 1.5);

-- --------------------------------------------------------

--
-- Table structure for table `parities`
--

CREATE TABLE `parities` (
  `id` int(11) NOT NULL,
  `exchange` int(11) NOT NULL,
  `parity` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parities`
--

INSERT INTO `parities` (`id`, `exchange`, `parity`) VALUES
(1, 1, 'BTC/USDT'),
(2, 1, 'ETH/USDT'),
(3, 1, 'LTC/USDT'),
(4, 1, 'XRP/USDT'),
(5, 1, 'ETH/BTC'),
(6, 1, 'LTC/BTC'),
(7, 1, 'XRP/BTC'),
(8, 1, 'BTG/USDT'),
(9, 1, 'ETC/USDT'),
(10, 1, 'BCC/USDT'),
(11, 1, 'BTG/BTC'),
(12, 1, 'ETC/BTC'),
(13, 1, 'XEM/BTC'),
(14, 1, 'BCC/BTC'),
(15, 1, 'DASH/BTC'),
(16, 2, 'BTC/TRY'),
(17, 2, 'BTC/USDT'),
(18, 2, 'ETH/TRY'),
(19, 2, 'LTC/TRY'),
(20, 2, 'XRP/TRY'),
(21, 2, 'USDT/TRY'),
(22, 3, 'BTC/TRY'),
(23, 3, 'BTC/USDT'),
(24, 3, 'ETH/TRY'),
(25, 3, 'LTC/TRY'),
(26, 3, 'XRP/TRY'),
(27, 3, 'USDT/TRY'),
(28, 3, 'DASH/TRY'),
(29, 3, 'XLM/TRY'),
(30, 3, 'BTG/TRY'),
(31, 3, 'ETC/TRY'),
(32, 3, 'XEM/TRY'),
(33, 3, 'DOGE/TRY'),
(34, 3, 'BCH/TRY');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `cdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `cdate`) VALUES
(1, 'admin', 'admin', '2018-11-08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `limits`
--
ALTER TABLE `limits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parities`
--
ALTER TABLE `parities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `limits`
--
ALTER TABLE `limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `parities`
--
ALTER TABLE `parities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
