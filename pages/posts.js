import { Component } from 'react'
import { Popover, Button } from 'antd';

export default class extends Component {
    state = {
      visible: false,
    }
  
    hide = () => {
      // this.setState({
      //   visible: false,
      // });
    }
  
    hsv = (v) => {
      // console.log(v)
      this.setState({ visible:!this.state.visible });
    }
  
    render() {
      return (
        <Popover
          content={<a onClick={this.hide}>Close</a>}
          title="Title"
          trigger="click"
          visible={this.state.visible}
          onVisibleChange={() => this.hsv([this.state.visible])}
        >
          <Button type="primary">Click me</Button>
        </Popover>
      );
    }
  }