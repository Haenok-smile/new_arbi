import { Component } from 'react'
import { Form, Icon, Input, Button } from 'antd';
import { Row, Col } from 'antd'
import '../static/css/style.css'

const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class HorizontalLoginForm extends React.Component {
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        location.href='/signin/' + values.userName + '/' + values.password
      }
    });
  }

  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const userNameError = isFieldTouched('userName') && getFieldError('userName');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    return (
      <Form layout="inline" onSubmit={this.handleSubmit} style={{width:600,height:200,background:'#FFF',margin:'0 auto',textAlign:'center'}}>
        <div style={{height:90,background:'#003351',marginBottom:30,position:'relative'}}>            
            <div style={{width:100,height:100,borderRadius:50,background:'#003351',position:'absolute',top:-10,left:250,paddingTop:10}}>
                <Icon type="dollar" style={{fontSize:80,color:'#FFF'}}/>
            </div>
        </div>
        <FormItem
          validateStatus={userNameError ? 'error' : ''}
          help={userNameError || ''}
        >
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
          )}
        </FormItem>
        <FormItem
          validateStatus={passwordError ? 'error' : ''}
          help={passwordError || ''}
        >
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
          )}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
          >
            Sign in
          </Button>
        </FormItem>
      </Form>
    );
  }
}
const WrappedHorizontalLoginForm = Form.create()(HorizontalLoginForm);
export default() => (
    <Row>
        <Col span={24} style={{paddingTop:200}}>
            <WrappedHorizontalLoginForm />
        </Col>
    </Row>
)