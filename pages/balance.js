import { Component } from 'react'
import { Menu, Icon, Affix, Spin } from 'antd'
import { Row, Col } from 'antd'
import { Table } from 'antd';
import '../static/css/style.css'

const SubMenu = Menu.SubMenu;

const columns = [{
    title: '',
    dataIndex: 'key',
  }, {
    title: 'Coin',
    dataIndex: 'name',
  }, {
    title: 'Address',
    dataIndex: 'address',
  }, {
    title: 'Binance Balance',
    dataIndex: 'balance1'
  }, {
    title: 'Binance OnTrade',
    dataIndex: 'ontrade1'
  }, {
    title: 'Btcturk Balance',
    dataIndex: 'balance2'
  }, {
    title: 'Btcturk OnTrade',
    dataIndex: 'ontrade2'
  }, {
    title: 'Remark'
}];

export default class extends Component {
    state = {
        balancedata: null
    }
    componentDidMount = () => {
        fetch('/balance-list')
        .then(res => res.json())
        .then(data => {
            let balancedata = [];
            let i = 1;
            for (let key of Object.keys(data.binance)) {
                let obj = {};
                obj.key = i;
                obj.name = key;
                obj.address = '860001163258741328321324899985510';
                obj.balance1 = data.binance[key].available;
                obj.ontrade1 = data.binance[key].onOrder;
                obj.balance2 = '';
                obj.ontrade2 = '';
                balancedata.push(obj);
                i++;
            }
            this.setState({
                balancedata: balancedata
            })
        })
        .catch((error) => {})
    }
    render() {
        if (this.state.balancedata) {
            var Tableview = () => <Table columns={columns} dataSource={this.state.balancedata} size="small" style={{background:'#FFF'}}/>
        } else {
            var Tableview = () => <Row style={{height:447,background:'#FFF',textAlign:'center',paddingTop:200}}><Spin /></Row>
        }
        return (
            <div>
                <style jsx>{`
                    td {
                        text-align: left;
                    }
                `}</style>
                <Menubar current='balance'/>
                <Tableview />
            </div>
        )
    }
}
const Menubar = () => (
    <Affix offsetTop={0}>
        <Menu
            selectedKeys={['balance']}
            mode="horizontal"
            theme='dark'
            style={{padding:'10px 0'}}>
            <Menu.Item key="exchange">
                <a href="/index">
                    <Icon type="appstore" />Exchange
                </a>
            </Menu.Item>                        
            <Menu.Item key="currency">
                <a href="/currency">
                    <Icon type="property-safety" />Currency
                </a>
            </Menu.Item>                        
            <Menu.Item key="parity">
                <a href="/parity">
                    <Icon type="stock" />Parity
                </a>
            </Menu.Item>
            <Menu.Item key="balance">
                <a href="/balance">
                    <Icon type="strikethrough" />Balance
                </a>
            </Menu.Item>
            <SubMenu style={{float:'right',padding:0}} title={<span className="submenu-title-wrapper"><Icon type="team" style={{fontSize:20}} /></span>}>
                <Menu.Item key="user:1" style={{margin:0,background:'#00101a'}}><Icon type="user"/>John</Menu.Item>
                <Menu.Item key="user:2" style={{margin:0,background:'#00101a'}}><Icon type="user"/>Kerry</Menu.Item>
                <Menu.Item key="user:3" style={{margin:0,background:'#00101a'}}><Icon type="user"/>Volten</Menu.Item>
            </SubMenu>
        </Menu>
    </Affix>
)
