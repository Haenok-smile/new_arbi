import { Component } from 'react'
import { Row, Col } from 'antd'
import { Input, InputNumber, AutoComplete, Select, Button, Menu, Icon, Popconfirm, message, Affix, Alert } from 'antd'
import '../static/css/style.css'

let bizcharts;
if (process.browser) {
  bizcharts = require('bizcharts')
}
import DataSet from "@antv/data-set"
  
const ButtonGroup = Button.Group;
const Option = Select.Option;
const InputGroup = Input.Group;
const SubMenu = Menu.SubMenu;

const datacollection = {
    BINANCE: [
            'BTC/USDT', 'ETH/USDT', 'LTC/USDT', 'XRP/USDT', 'ETH/BTC', 'LTC/BTC', 'XRP/BTC', 'BTG/USDT', 'ETC/USDT', 'BCC/USDT', 'BTG/BTC', 'ETC/BTC', 'XEM/BTC', 'BCC/BTC', 'DASH/BTC'    
        ],
    BTCTURK: [
            'BTC/TRY', 'BTC/USDT', 'ETH/TRY', 'LTC/TRY', 'XRP/TRY', 'USDT/TRY'
        ]
}
const autocompletedata = [
    'BTC/TL/USDT',
    'ETH/TL/USDT',
    'LTC/TL/USDT',
    'XRP/TL/USDT',
    'ETH/TL/BTC',
    'LTC/TL/BTC',
    'XRP/TL/BTC',
    'BTG/TL/USDT',
    'ETC/TL/USDT',
    'BCC/TL/USDT',
    'BTG/TL/BTC',
    'ETC/TL/BTC',
    'XEM/TL/BTC',
    'BCC/TL/BTC',
    'DASH/TL/BTC'
]

class Paritybox extends React.Component {
    state = {
        exchanges: ['BINANCE', 'BINANCE', 'BINANCE'],
        parities : ['BTC/USDT', 'BTC/USDT', 'BTC/USDT'],
        markets: ['STD'],
        order: 'STD'
    }
    subexchange = (value, index) => {
        let T = this.state.exchanges;
        T[index] = value;
        let P = this.state.parities;
        P[index] = datacollection[value][0];
        this.setState({
            exchanges: T,
            parities: P
        })
        
    }
    paritychange = (value, index) => {
        let P = this.state.parities;
        P[index] = value;
        this.setState({
            parities: P
        })
    }
    marketchange = (value) => {
        if (value === 'market')
            var T = ['STD'];
        else
            var T = ['STD', 'WALL'];

        this.setState({
            markets: T,
            order: 'STD'
        })
    }
    orderchange = (value) => {
        this.setState({
            order: value
        })
    }
    
    render() {
        return (
            <Row style={{marginBottom:5}}>
                <Col span={24} style={{}}>
                    <Row style={{background:'#00395b'}}>
                        <Col span={5} style={{height:295,borderRight:'solid #EEE 1px',background:'#FFF'}}>
                            <Row style={{padding:'3px 3px 2px 3px'}}>
                                <Col span={24}>                                    
                                    <div style={{width:63,display:'inline-block'}}>
                                        <ButtonGroup>
                                            <Button icon="plus" onClick={this.props.boxadd} />
                                            <Popconfirm placement="topLeft" title='Are you sure to delete this box?' onConfirm={this.props.boxremove} okText="Yes" cancelText="No">
                                                <Button icon="minus" />
                                            </Popconfirm>
                                        </ButtonGroup>
                                    </div>
                                    <div style={{width:'calc(100% - 66px)',display:'inline-block',marginLeft:3}}>
                                        <AutoComplete
                                            dataSource={this.props.filterdata}
                                            // onSelect={onSelect}
                                            onSearch={this.props.autocomplete}
                                            style={{width:'100%'}}//width:'48.9%',float:'right',
                                        />
                                    </div>
                                </Col>
                            </Row>
                            <Row style={{padding:'0 3px'}}>
                                <InputGroup compact>
                                    <Select defaultValue="trk" size='small' style={{width:'50%'}}>
                                        <Option value="btc">BINANCE</Option>
                                        <Option value="trk">BTCTURK</Option>
                                    </Select>
                                    <Select defaultValue="btc" size='small' style={{width:'50%'}}>
                                        <Option value="btc">BINANCE</Option>
                                        <Option value="trk">BTCTURK</Option>
                                    </Select>
                                </InputGroup>
                            </Row>
                            <Row style={{padding:'3px 3px 0 3px'}}>
                                <InputGroup compact>
                                    <Select size='small' defaultValue="buy" style={{width:'25%'}}>
                                        <Option value="sell">SELL</Option>
                                        <Option value="buy">BUY</Option>
                                    </Select>
                                    <Select size='small' value={this.state.exchanges[0]} style={{width:'40%'}} onChange={(e) => this.subexchange(e, 0)}>
                                        <Option value="BINANCE">BINANCE</Option>
                                        <Option value="BTCTURK">BTCTURK</Option>
                                    </Select>
                                    <Select size='small' value={this.state.parities[0]} style={{width:'35%'}} onChange={(e) => this.paritychange(e, 0)}>
                                        {
                                            datacollection[this.state.exchanges[0]].map((parity) => <Option value={parity} key={parity}>{parity}</Option>)
                                        }
                                    </Select>
                                </InputGroup>
                            </Row>
                            <Row style={{padding:'3px 3px 0 3px'}}>
                                <InputGroup compact>
                                    <Select size='small' defaultValue="buy" style={{width:'25%'}}>
                                        <Option value="sell">SELL</Option>
                                        <Option value="buy">BUY</Option>
                                    </Select>
                                    <Select size='small' value={this.state.exchanges[1]} style={{width:'40%'}} onChange={(e) => this.subexchange(e, 1)}>
                                        <Option value="BINANCE">BINANCE</Option>
                                        <Option value="BTCTURK">BTCTURK</Option>
                                    </Select>
                                    <Select size='small' value={this.state.parities[1]} style={{width:'35%'}} onChange={(e) => this.paritychange(e, 1)}>
                                        {
                                            datacollection[this.state.exchanges[1]].map((parity) => <Option value={parity} key={parity}>{parity}</Option>)
                                        }
                                    </Select>
                                </InputGroup>
                            </Row>
                            <Row style={{padding:'3px 3px 0 3px'}}>
                                <InputGroup compact>
                                    <Select size='small' defaultValue="buy" style={{width:'25%'}}>
                                        <Option value="sell">SELL</Option>
                                        <Option value="buy">BUY</Option>
                                    </Select>
                                    <Select size='small' value={this.state.exchanges[2]} style={{width:'40%'}} onChange={(e) => this.subexchange(e, 2)}>
                                        <Option value="BINANCE">BINANCE</Option>
                                        <Option value="BTCTURK">BTCTURK</Option>
                                    </Select>
                                    <Select size='small' value={this.state.parities[2]} style={{width:'35%'}} onChange={(e) => this.paritychange(e, 2)}>
                                        {
                                            datacollection[this.state.exchanges[2]].map((parity) => <Option value={parity} key={parity}>{parity}</Option>)
                                        }
                                    </Select>
                                </InputGroup>
                            </Row>
                            <Row style={{padding:'3px 3px 0 3px'}}>
                                <InputGroup compact>
                                    <InputNumber
                                        size='small'
                                        defaultValue={1000}
                                        formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                        style={{width:'50%'}}
                                    />
                                    <Button size='small' type='primary' style={{width:'50%'}}>+12</Button>
                                </InputGroup>
                            </Row>
                            <Row style={{padding:'3px 3px 0 3px'}}>
                                <InputGroup compact>
                                    <Select size='small' defaultValue="market" style={{width:'65%'}} onChange={this.marketchange}>
                                        <Option value="market">MARKET</Option>
                                        <Option value="limit">LIMIT</Option>
                                    </Select>
                                    <Select size='small' value={this.state.order} style={{width:'35%'}} onChange={this.orderchange}>
                                        {
                                            this.state.markets.map((order, i) => <Option value={order} key={i}>{order}</Option>)
                                        }
                                    </Select>
                                </InputGroup>
                            </Row>
                            <Row style={{padding:'3px 3px 0 3px'}}>
                                <Col span={16} style={{textAlign:'center'}}>
                                    AUTO
                                </Col>
                                <Col span={8}>
                                    <Select size='small' defaultValue="no" style={{width:'100%'}} disabled>
                                        <Option value="yes">YES</Option>
                                        <Option value="no">NO</Option>
                                    </Select>
                                </Col>
                            </Row>
                            <Row style={{padding:'3px 3px 0 3px'}}>
                                <Col span={16} style={{textAlign:'center'}}>
                                    INVENTORY
                                </Col>
                                <Col span={8}>
                                    <Select size='small' defaultValue="no" style={{width:'100%'}} disabled>
                                        <Option value="yes">YES</Option>
                                        <Option value="no">NO</Option>
                                    </Select>
                                </Col>
                            </Row>
                            <Row style={{padding:'3px 3px 0 3px'}}>
                                <Col span={16} style={{textAlign:'center'}}>
                                    WITHDRAW
                                </Col>
                                <Col span={8}>
                                    <Select size='small' defaultValue="no" style={{width:'100%'}} disabled>
                                        <Option value="yes">YES</Option>
                                        <Option value="no">NO</Option>
                                    </Select>
                                </Col>
                            </Row>
                        </Col>
                        <Col span={11} style={{height:295,padding:'15px 20px 0 0',background:'#FFF'}}>
                            {process.browser &&
                                <bizcharts.Chart height={275} data={this.props.dv} forceFit>
                                    <bizcharts.Legend />
                                    <bizcharts.Axis name="月份" />
                                    <bizcharts.Axis name="月均降雨量" />
                                    <bizcharts.Tooltip />
                                    <bizcharts.Geom
                                        type="intervalStack"
                                        position="月份*月均降雨量"
                                        color={['name', ['lightgreen', 'green', 'pink', 'red']]}
                                        style={{
                                        stroke: "#fff",
                                        lineWidth: 1
                                        }}
                                    />
                                </bizcharts.Chart>
                            }
                        </Col>
                        <Col span={4} style={{height:295,paddingTop:5,overflow:'auto',background:'#FFF'}}>
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                            <Alert
                                message="buy-20.03.08-xrp-300 usd"
                                type="info"
                                closable
                                closeText="Cancel"
                                style={{marginBottom:3,paddingTop:5,paddingBottom:5}}
                            />
                        </Col>
                        <Col span={4} style={{height:295,overflowY:'auto',padding:'0 5px',msScrollbarFaceColor:'red'}}>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>12$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>2$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>10$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>8$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>12$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>12$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>5$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>10$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>11$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>6$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>7$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>9$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>12$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>10$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>11$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>2$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>7$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>8$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>8$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12,background:'#014f7c'}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>6$</span></p>
                            <p style={{padding:0,margin:0,color:'#FFF',fontSize:12}}>20.03.08 15:04:53 buy-xrp-300<span style={{float:'right'}}>10$</span></p>
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default class extends Component {
    state = {
        parityboxes: [''],
        filterdata: []
    }    
    boxadd = (e) => {
        let T = this.state.parityboxes;
        T.push('');
        this.setState({
            parityboxes: T
        })
        message.success('A new box created.');
    }
    boxremove = () => {
        let T = this.state.parityboxes;
        if (T.length == 1) {
            message.warning('Sorry. The last box cannot be removed.');
            return;
        }
        T.splice(0, 1);
        this.setState({
            parityboxes: T
        })
        message.success('The selected box removed.');
    }
    autocomplete = (value) => {
        if (value == '')
            this.setState({
                filterdata: []
            })
        else
            this.setState({
                filterdata: autocompletedata.filter((s) => s.toLowerCase().substring(0, value.length) === value.toLowerCase())
            })
    }
    render() {
        const data = [
            {
                name: "limit win",
                "1": 2.5,
                "3": 2,
                "5": 1.3,
                "6": 1.4,
                "9": .7,
                "11": .3,
                "13": 2.4,
                "15": .6,
                "17": 2.3,
                "19": 1.4,
                "21": .6,
                "23": 1.6
            },
            {
                name: "market win",
                "1": 5.4,
                "3": 3.2,
                "5": 5.5,
                "6": 6.7,
                "9": 2.6,
                "11": 5.5,
                "13": 7.4,
                "15": 2.4,
                "17": 5.3,
                "19": 2.4,
                "21": 5.6,
                "23": 5.6
            },
            {
                name: "limit lose",
                "2": -2.4,
                "4": -2.2,
                "7": -1.5,
                "8": -1.7,
                "10": -1.6,
                "12": -2.5,
                "14": -2.4,
                "16": -1.4,
                "18": -2.3,
                "20": -2.4,
                "22": -1.6,
                "24": -0.6
            },
            {
                name: "market lose",
                "2": -5.3,
                "4": -6.8,
                "7": -6.3,
                "8": -7.4,
                "10": -4.7,
                "12": -2.3,
                "14": -4.4,
                "16": -5.6,
                "18": -2.3,
                "20": -5.4,
                "22": -3.6,
                "24": -5.6
            }
          ];
        const ds = new DataSet();
        const dv = ds.createView().source(data);
        dv.transform({
            type: "fold",
            fields: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"],
            key: "月份",
            value: "月均降雨量"
        });
        return (
            <div>
                <Row>
                    <Affix offsetTop={0}>
                        <Menu
                            selectedKeys={['parity']}
                            mode="horizontal"
                            theme='dark'
                            style={{padding:'10px 0'}}>
                            <Menu.Item key="exchange">
                                <a href="/index">
                                    <Icon type="appstore" />Exchange
                                </a>
                            </Menu.Item>                        
                            <Menu.Item key="currency">
                                <a href="/currency">
                                    <Icon type="property-safety" />Currency
                                </a>
                            </Menu.Item>                        
                            <Menu.Item key="parity">
                                <a href="/parity">
                                    <Icon type="stock" />Parity
                                </a>
                            </Menu.Item>
                            <Menu.Item key="balance">
                                <a href="/balance">
                                    <Icon type="strikethrough" />Balance
                                </a>
                            </Menu.Item>
                            <SubMenu style={{float:'right',padding:0}} title={<span className="submenu-title-wrapper"><Icon type="team" style={{fontSize:20}} /></span>}>
                                <Menu.Item key="user:1" style={{margin:0,background:'#00101a'}}><Icon type="user"/>John</Menu.Item>
                                <Menu.Item key="user:2" style={{margin:0,background:'#00101a'}}><Icon type="user"/>Kerry</Menu.Item>
                                <Menu.Item key="user:3" style={{margin:0,background:'#00101a'}}><Icon type="user"/>Volten</Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Affix>
                </Row>
                {
                    this.state.parityboxes.map((box, i) => 
                        <Paritybox 
                            key={i}
                            // value props
                            dv           = {dv}
                            filterdata   = {this.state.filterdata}
                            // action props
                            boxadd       = {this.boxadd}
                            boxremove    = {this.boxremove}
                            autocomplete = {this.autocomplete}
                        />
                    )
                }
            </div>
        )
    }
}