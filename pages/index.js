import { Component } from 'react'
import { Row, Col } from 'antd'
import { Select, Button, Input, InputNumber, Menu, Icon, Popconfirm, Affix, message, Tooltip, Popover, Spin, notification } from 'antd'
import '../static/css/style.css'
let bizcharts;
if (process.browser) {
  bizcharts = require('bizcharts')
}
const Option = Select.Option;
const ButtonGroup = Button.Group;
const InputGroup = Input.Group;
const SubMenu = Menu.SubMenu;

const datacollection = {
    BINANCE: [
            'BTC/USDT', 'ETH/USDT', 'LTC/USDT', 'XRP/USDT', 'ETH/BTC', 'LTC/BTC', 'XRP/BTC', 'BTG/USDT', 'ETC/USDT', 'BCC/USDT', 'BTG/BTC', 'ETC/BTC', 'XEM/BTC', 'BCC/BTC', 'DASH/BTC'    
        ],
    BTCTURK: [
            'BTC/TRY', 'BTC/USDT', 'ETH/TRY', 'LTC/TRY', 'XRP/TRY', 'USDT/TRY'
        ],
    KOINEKS: [
            'BTC/TRY', 'BTC/USDT', 'ETH/TRY', 'LTC/TRY', 'XRP/TRY', 'USDT/TRY', 'DASH/TRY', 'XLM/TRY', 'BTG/TRY', 'ETC/TRY', 'XEM/TRY', 'DOGE/TRY', 'BCH/TRY'    
        ]
}
////////////////////
var chartdata = [
    {
      name: "maker",
      vote: 0.032
    },
    {
      name: "taker",
      vote: 0.028
    }
];
const chartscale = {
    vote: {
      min: 0
    }
};
  
////////////////////////////////      COMPONENT   >>>>>>    //////////////////////////////////////////
const Menubar = () => (
    <Affix offsetTop={0}>
        <Menu
            selectedKeys={['exchange']}
            mode="horizontal"
            theme='dark'
            style={{padding:'10px 0'}}>
            <Menu.Item key="exchange">
                <a href="/index">
                    <Icon type="appstore" />Exchange
                </a>
            </Menu.Item>                        
            <Menu.Item key="currency">
                <a href="/currency">
                    <Icon type="property-safety" />Currency
                </a>
            </Menu.Item>                        
            <Menu.Item key="parity">
                <a href="/parity">
                    <Icon type="stock" />Parity
                </a>
            </Menu.Item>
            <Menu.Item key="balance">
                <a href="/balance">
                    <Icon type="strikethrough" />Balance
                </a>
            </Menu.Item>
            <SubMenu style={{float:'right',padding:0}} title={<span className="submenu-title-wrapper"><Icon type="team" style={{fontSize:20}} /></span>}>
                <Menu.Item key="user:1" style={{margin:0,background:'#00101a'}}><Icon type="user"/>John</Menu.Item>
                <Menu.Item key="user:2" style={{margin:0,background:'#00101a'}}><Icon type="user"/>Kerry</Menu.Item>
                <Menu.Item key="user:3" style={{margin:0,background:'#00101a'}}><Icon type="user"/>Volten</Menu.Item>
            </SubMenu>
        </Menu>
    </Affix>
)
const Headbar = (props) => (
    <Row>
        <Col span={24} style={{padding:3}}>
            <ButtonGroup>
                <Button icon="plus" onClick={props.boxadd} disabled={props.selectdisble} />
                <Popconfirm placement="topLeft" title='Are you sure to delete this box?' onConfirm={() => props.boxremove(props.boxkey)} okText="Yes" cancelText="No">
                    <Button icon="minus" disabled={props.selectdisble} />
                </Popconfirm>
            </ButtonGroup>
            <Select value={props.parity} style={{ width:120,float:'right' }} onChange={(e) => props.parityChange(props.boxkey, e)}>
                {datacollection[props.exchange].map(parity => <Option value={parity} key={parity}>{parity}</Option>)}
            </Select>
            <Select value={props.exchange} style={{ width:120,float:'right',marginRight:3 }} onChange={(e) => props.exchangeChange(props.boxkey, e)}>
                {
                    Object.keys(datacollection).map(exchange => (
                        <Option value={exchange} key={exchange}>{exchange}</Option>        
                    ))
                }
            </Select>
        </Col>
    </Row>
)
const Contentbar = (props) => {      
    const EPstate = props.EPstate;
    if (EPstate) {
        switch (props.exchange) {
            case 'BINANCE':
                if (Object.keys(EPstate.depth).length===0 && EPstate.depth.constructor===Object) {
                    var Buydata = () => <tr><td colSpan='3'><Spin /></td></tr>;
                    var Selldata = Buydata;
                } else {
                    var Buydata = () =>
                        (Object.keys(EPstate.depth.bids).slice(0, 18).map((item, i) => (
                            <tr className='trrow' key={i}>
                                <td>{ parseFloat(item).toFixed(4) }</td>
                                <td>{ parseFloat(EPstate.depth.bids[item]).toFixed(2) }</td>
                                <td>{ (parseFloat(item).toFixed(4) * parseFloat(EPstate.depth.bids[item]).toFixed(4)).toFixed(2) }</td>
                            </tr>
                        )))
                    var Selldata = () =>
                        (Object.keys(EPstate.depth.asks).slice(0, 18).map((item, i) => (
                            <tr className='trrow' key={i}>
                                <td>{ parseFloat(item).toFixed(4) }</td>
                                <td>{ parseFloat(EPstate.depth.asks[item]).toFixed(2) }</td>
                                <td>{ (parseFloat(item).toFixed(4) * parseFloat(EPstate.depth.asks[item]).toFixed(4)).toFixed(2) }</td>
                            </tr>
                        )))
                }
                if (Object.keys(EPstate.history).length===0 && EPstate.history.constructor===Object) {
                    var Historydata = () => <tr><td colSpan='3'><Spin /></td></tr>;
                } else {
                    var Historydata = () =>
                        (EPstate.history.map((item, i) => (
                            <tr className='trrow' key={i}>
                                <td>{ parseFloat(item.p).toFixed(4) }</td>
                                <td>{ parseFloat(item.q).toFixed(2) }</td>
                                <td>{ (new Date(item.T*1000)).getHours()+':'+(new Date(item.T*1000)).getMinutes()+':'+(new Date(item.T*1000)).getSeconds() }</td>
                            </tr>                                                        
                        )))
                }
                break;
            case 'BTCTURK':
                var Buydata = () => 
                    (EPstate.bids.slice(0, 18).map((item, i) => (
                        <tr className='trrow' key={i}>
                            <td>{ parseFloat(item[0]).toFixed(4) }</td>
                            <td>{ parseFloat(item[1]).toFixed(2) }</td>
                            <td>{ (parseFloat(item[0]).toFixed(4) * parseFloat(item[1]).toFixed(4)).toFixed(2) }</td>
                        </tr>                                                        
                    )))
                var Selldata = () => 
                    (EPstate.asks.slice(0, 18).map((item, i) => (
                        <tr className='trrow' key={i}>
                            <td>{ parseFloat(item[0]).toFixed(4) }</td>
                            <td>{ parseFloat(item[1]).toFixed(2) }</td>
                            <td>{ (parseFloat(item[0]).toFixed(4) * parseFloat(item[1]).toFixed(4)).toFixed(2) }</td>
                        </tr>                                                        
                    )))
                var Historydata = () =>
                    (props.EPsh.map((item, i) => (
                        <tr className='trrow' key={i}>
                            <td>{ parseFloat(item.price).toFixed(4) }</td>
                            <td>{ parseFloat(item.amount).toFixed(2) }</td>
                            <td>{ (new Date(item.date)).getHours()+':0'+(new Date(item.date)).getMinutes()+':0'+(new Date(item.date)).getSeconds() }</td>
                        </tr>                                                        
                    )))
                break;
            case 'KOINEKS':                
                let a = props.parity;
                let b = a.split('/');
                let c = b[0];
                var Buydata = () =>
                    <tr className='trrow'>
                        <td>{ parseFloat(EPstate[c].bid).toFixed(4) }</td>
                        <td>{ parseFloat(EPstate[c].change_amount).toFixed(2) }</td>
                        <td>{ (parseFloat(EPstate[c].bid).toFixed(4)*parseFloat(EPstate[c].change_amount)).toFixed(2) }</td>
                    </tr>
                var Selldata = () =>
                    <tr className='trrow'>
                        <td>{ parseFloat(EPstate[c].ask).toFixed(4) }</td>
                        <td>{ parseFloat(EPstate[c].change_amount).toFixed(2) }</td>
                        <td>{ (parseFloat(EPstate[c].ask).toFixed(4)*parseFloat(EPstate[c].change_amount)).toFixed(2) }</td>
                    </tr>
                var Historydata = () => <tr><td colSpan='3'>No History Data</td></tr>;
                break;
        }
    } else {
        var Buydata = () => <tr><td colSpan='3'><Spin /></td></tr>;
        var Selldata = Buydata;
        var Historydata = Buydata;    
    }
    return (
        <Row gutter={3} style={{padding:'0 3px'}}>
            <Col span={8}>
                <div style={{height:400,border:'solid #EEE 1px',textAlign:'center'}}>
                    <div style={{padding:'4px 0',borderBottom:'solid #1890ff 1px',color:'#1890ff'}}>
                        <Icon type="fork" theme="outlined" style={{marginRight:5}} />Buy Orders
                    </div>
                    <div style={{height:370,overflow:'auto'}}>
                        <table cellPadding='0' cellSpacing='0'>
                            <tbody>
                                <tr className='trrow head'><td>Price</td><td>Amout</td><td>Total</td></tr>
                                <Buydata />
                                <tr><td colSpan='3'></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </Col>
            <Col span={8}>
                <div style={{height:400,border:'solid #EEE 1px',textAlign:'center'}}>
                    <div style={{padding:'4px 0',borderBottom:'solid #1890ff 1px',color:'#1890ff'}}>
                        <Icon type="fork" theme="outlined" style={{marginRight:5}} />Sell Orders
                    </div>
                    <div style={{height:370,overflow:'auto'}}>
                        <table cellPadding='0' cellSpacing='0'>
                            <tbody>
                                <tr className='trrow head'><td>Price</td><td>Amout</td><td>Total</td></tr>
                                <Selldata />
                                <tr><td colSpan='3'></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </Col>
            <Col span={8}>
                <div style={{height:400,border:'solid #EEE 1px',textAlign:'center'}}>
                    <div style={{padding:'4px 0',borderBottom:'solid #1890ff 1px',color:'#1890ff'}}>
                        <Icon type="fork" theme="outlined" style={{marginRight:5}} />Order History
                    </div>
                    <div style={{height:370,overflow:'auto'}}>
                        <table cellPadding='0' cellSpacing='0'>
                            <tbody>
                                <tr className='trrow head'><td>Price</td><td>Amout</td><td>Time</td></tr>
                                <Historydata />
                                <tr><td colSpan='3'></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </Col>
        </Row>
    )
}

const Chartbar = (props) => {
    let a = props.parity.split('/');
    var r1, r2;
    
    if (props.balancedata && props.balancedata[props.exchange] && props.pricedata && props.pricedata[props.exchange]) {

        if (props.balancedata[props.exchange][a[0]] && props.balancedata[props.exchange][a[1]]) {
            let b1 = parseFloat(props.balancedata[props.exchange][a[0]].available);
            let b2 = parseFloat(props.balancedata[props.exchange][a[1]].available);

            r1 = b1 * parseFloat(props.pricedata[props.exchange][a[0] + 'USDT']);
            if (a[1] == 'USDT')
                r2 = b2;
            else
                r2 = b2 * parseFloat(props.pricedata[props.exchange][a[1] + 'USDT']);
            
            r1 = parseFloat(r1.toFixed(2));
            r2 = parseFloat(r2.toFixed(2));
            
            if (r1 == 0) r1 = .01;
            if (r2 == 0) r2 = .01;
        }
    }

    const EPstate = props.EPstate;
    if (EPstate && props.balancedata && r1 && r2) {
        const piecolors = ['#ff7202', '#5ca536'];
        var Piechart = () => <Pie
            data={[r1, r2]}
            radius={ 60 }
            hole={ 0 }
            colors={ piecolors }
            strokeWidth={ 3 }
            labels={ true }
        />
    } else {
        var Piechart = () => <Spin style={{marginTop:50}}/>;
    }
    switch (props.exchange) {
        case 'BINANCE':                          
            var Feechart = () => <Pie
                data={[.075, .075]}
                radius={ 40 }
                hole={ 0 }
                colors={ ['#78bed6', '#ff6f88'] }
                strokeWidth={ 3 }
                labels={ true }
            />
            break;
        case 'BTCTURK':
            var Feechart = () => <Pie
                data={[.1, .04]}
                radius={ 40 }
                hole={ 0 }
                colors={ ['#78bed6', '#ff6f88'] }
                strokeWidth={ 3 }
                labels={ true }
            />
            break;
        default:
            var Feechart = () => <Spin />;
    }
    const apikeybox = (
    <Input
        style={{width:300}}
        placeholder="API key"
        onKeyDown={(e) => props.popKeyEnter(e, props.boxkey, 1)}
    />
    );
    const secretkeybox = (
        <Input
            style={{width:300}}
            placeholder="Secret key"
            onKeyDown={(e) => props.popKeyEnter(e, props.boxkey, 2)}
        />
    );
    const coin1keybox = (
        <Input
            style={{width:200}}
            placeholder={props.parity.split('/')[0] + ' key'}
            onKeyDown={(e) => props.popKeyEnter(e, props.boxkey, 3)}
        />
    );
    const coin2keybox = (
        <Input
            style={{width:200}}
            placeholder={props.parity.split('/')[1] + ' key'}
            onKeyDown={(e) => props.popKeyEnter(e, props.boxkey, 4)}
        />
    );

    return (
        <Row style={{padding:'20px 0'}}>
            <Col span={7} style={{textAlign:'center'}}>
                <Piechart />
            </Col>
            <Col span={11}>
                <ul style={{height:27,marginBottom:0}}>
                    <li style={{width:'50%',float:'left',whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                        balance warning
                    </li>
                    <li style={{width:'50%',float:'left'}}>
                    <InputNumber
                        value={props.paritylimits[props.exchange][props.parity] ? props.paritylimits[props.exchange][props.parity][0] : 0}
                        formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        style={{width:'100%'}}
                        size='small'
                        onChange = {(e) => props.limitchange(e, props.exchange, props.parity, 0)}
                    />    
                    </li>
                </ul>
                <ul style={{height:27,marginBottom:0}}>
                    <li style={{width:'50%',float:'left',whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                        ratio warning %
                    </li>
                    <li style={{width:'50%',float:'left'}}>
                    <InputNumber
                        value={props.paritylimits[props.exchange][props.parity] ? props.paritylimits[props.exchange][props.parity][1] : 0}
                        min={0}
                        max={100}
                        formatter={value => `${value}%`}
                        parser={value => value.replace('%', '')}
                        step={.1}
                        style={{width:'100%'}}
                        size='small'
                        key='special'
                        onChange = {(e) => props.limitchange(e, props.exchange, props.parity, 1)}
                    />
                    </li>
                </ul>
                <ul style={{height:27,marginBottom:0}}>
                    <li>
                        <Tooltip placement="right" title='API key'>
                            <Input size='small' type='password'
                                value={props.apikeys[props.exchange]}
                                suffix = {<Icon type="pushpin" style={{ color: 'rgba(0,0,0,.25)' }}/>}
                                style = {{display:props.apikeys[props.exchange] == '' ? 'none' : 'block'}}
                                disabled
                            />
                        </Tooltip>
                        <Popover
                            trigger="click"
                            content={apikeybox}
                            visible={props.apikeypopover[props.boxkey]}
                            onVisibleChange={() => props.handleVisibleChange(props.boxkey, 1)}
                            >
                            <Button size='small' type='primary' style = {{display:props.apikeys[props.exchange] == '' ? 'block' : 'none'}} block>
                                API key
                            </Button>
                        </Popover>
                    </li>
                </ul>
                <ul style={{height:27,marginBottom:0}}>
                    <li>
                        <Tooltip placement="right" title='Secret key'>
                            <Input size='small' type='password'
                                value={props.secretkeys[props.exchange]}
                                suffix = {<Icon type="pushpin" style={{ color: 'rgba(0,0,0,.25)' }}/>}
                                style = {{display:props.secretkeys[props.exchange] == '' ? 'none' : 'block'}}
                                disabled
                            />
                        </Tooltip>
                        <Popover
                            trigger="click"
                            content={secretkeybox}
                            visible={props.secretkeypopover[props.boxkey]}
                            onVisibleChange={() => props.handleVisibleChange(props.boxkey, 2)}
                            >
                            <Button size='small' type='primary' style = {{display:props.secretkeys[props.exchange] == '' ? 'block' : 'none'}} block>
                                Secret key
                            </Button>
                        </Popover>
                    </li>
                </ul>
                <InputGroup style={{width:'100%'}} size='small'>
                    <Tooltip placement="bottom" title={props.parity.split('/')[0] + ' key'}>
                        <Input size='small' type='password'
                            value={props.coin1keys[props.boxkey]}
                            suffix = {<Icon type="pushpin" style={{ color: 'rgba(0,0,0,.25)' }}/>}
                            style = {{width:'50%',display:props.coin1keyinput[props.boxkey]}}
                            disabled
                        />
                    </Tooltip>
                    <Popover
                        trigger="click"
                        content={coin1keybox}
                        visible={props.coin1keypopover[props.boxkey]}
                        onVisibleChange={() => props.handleVisibleChange(props.boxkey, 3)}
                        >
                        <Button size='small' style={{width:'49.5%',display:props.coin1keybutton[props.boxkey]}}>
                            {props.parity.split('/')[0] + ' key'}
                        </Button>
                    </Popover>

                    <Tooltip placement="bottom" title={props.parity.split('/')[1] + ' key'}>
                        <Input size='small' type='password'
                            value={props.coin2keys[props.boxkey]}
                            suffix = {<Icon type="pushpin" style={{ color: 'rgba(0,0,0,.25)' }}/>}
                            style = {{width:'50%',float:'right',display:props.coin2keyinput[props.boxkey]}}
                            disabled
                        />
                    </Tooltip>
                    <Popover
                        trigger="click"
                        content={coin2keybox}
                        visible={props.coin2keypopover[props.boxkey]}
                        onVisibleChange={() => props.handleVisibleChange(props.boxkey, 4)}
                        >
                        <Button size='small' style={{width:'49.5%',float:'right',display:props.coin2keybutton[props.boxkey]}}>
                            {props.parity.split('/')[1] + ' key'}
                        </Button>
                    </Popover>
                </InputGroup>
            </Col>
            <Col span={6} style={{textAlign:'center'}}>
                <Feechart />
            </Col>
        </Row>
    )
}
class Exchangebox extends React.Component {
    render() {
        return (
            <Col span={8} className="exchangebox">
                <div>
                    <Headbar
                        boxkey              = {this.props.boxkey}
                        exchange            = {this.props.exchange}
                        parity              = {this.props.parity}
                        // action props
                        boxadd              = {this.props.boxadd}
                        boxremove           = {this.props.boxremove}
                        parityChange        = {this.props.parityChange}
                        exchangeChange      = {this.props.exchangeChange}
                    />
                    <Contentbar
                        boxkey              = {this.props.boxkey}
                        exchange            = {this.props.exchange}
                        parity              = {this.props.parity}
                        EPstate             = {this.props.EPstate}
                        EPsh                = {this.props.EPsh}
                    />
                    <Chartbar
                        // value props
                        boxkey              = {this.props.boxkey}
                        exchange            = {this.props.exchange}
                        parity              = {this.props.parity}
                        EPstate             = {this.props.EPstate}

                        apikeys             = {this.props.apikeys}
                        secretkeys          = {this.props.secretkeys}
                        paritylimits        = {this.props.paritylimits}

                        coin1keys           = {this.props.coin1keys}
                        coin2keys           = {this.props.coin2keys}
                        coin1keyinput       = {this.props.coin1keyinput}
                        coin2keyinput       = {this.props.coin2keyinput}
                        coin1keybutton      = {this.props.coin1keybutton}
                        coin2keybutton      = {this.props.coin2keybutton}

                        apikeypopover       = {this.props.apikeypopover}
                        secretkeypopover    = {this.props.secretkeypopover}
                        coin1keypopover     = {this.props.coin1keypopover}
                        coin2keypopover     = {this.props.coin2keypopover}

                        balancedata         = {this.props.balancedata}
                        pricedata           = {this.props.pricedata}
                        
                        // action props
                        popKeyEnter         = {this.props.popKeyEnter}
                        handleVisibleChange = {this.props.handleVisibleChange}
                        limitchange  = {this.props.limitchange}
                        playAudio           = {this.props.playAudio}
                    />
                </div>
            </Col>
        )
    }
}
////////////////////////////////  COMPONENT   <<<<<<       //////////////////////////////////////////
export default class extends Component {
    state = {
        selectdisble: true,

        // Initial - 1 element array
        exchangeboxes   : ['BINANCE'],
        parities        : ['BTC/USDT'],
        
        apikeys : {
            'BINANCE': '',
            'BTCTURK': '',
            'KOINEKS': ''
        },
        secretkeys : {
            'BINANCE': '',
            'BTCTURK': '',
            'KOINEKS': ''
        },
        paritylimits : {
            'BINANCE' : {},
            'BTCTURK' : {},
            'KOINEKS' : {}
        },

        coin1keys        : [''],
        coin2keys        : [''],        
        coin1keyinput    : ['none'],
        coin2keyinput    : ['none'],
        coin1keybutton   : ['inline'],
        coin2keybutton   : ['inline'],

        apikeypopover    : [false],
        secretkeypopover : [false],
        coin1keypopover  : [false],
        coin2keypopover  : [false],

        limitchangekey   : false,
        limitchangeparam : '',

        testtime : 0
    }
    boxadd = () => {        
        var T = this.state.exchangeboxes;
        var P = this.state.parities;
        var isNew = false;
        for (let key of Object.keys(datacollection)) {
            if (!this.state.exchangeboxes.includes(key)) {
                T.push(key);
                P.push(datacollection[key][0]);
                isNew = true;
                break;
            }
        }
        if (!isNew) {
            T.push('BINANCE');                
            P.push(datacollection['BINANCE'][0]);
        }
        let key_array_3 = this.state.coin1keys;          key_array_3.push('');
        let key_array_4 = this.state.coin2keys;          key_array_4.push('');
        let key_array_7 = this.state.coin1keyinput;      key_array_7.push('none');
        let key_array_8 = this.state.coin2keyinput;      key_array_8.push('none');
        let key_array_11 = this.state.coin1keybutton;    key_array_11.push('inline');
        let key_array_12 = this.state.coin2keybutton;    key_array_12.push('inline');
        let key_array_13 = this.state.apikeypopover;     key_array_13.push(false);
        let key_array_14 = this.state.secretkeypopover;  key_array_14.push(false);
        let key_array_15 = this.state.coin1keypopover;   key_array_15.push(false);
        let key_array_16 = this.state.coin2keypopover;   key_array_16.push(false);

        this.setState({
            exchangeboxes: T,
            parities: P,
            coin1keys:        key_array_3,
            coin2keys:        key_array_4,
            coin1keyinput:    key_array_7,
            coin2keyinput:    key_array_8,
            coin1keybutton:   key_array_11,
            coin2keybutton:   key_array_12,
            apikeypopover:    key_array_13,
            secretkeypopover: key_array_14,
            coin1keypopover:  key_array_15,
            coin2keypopover:  key_array_16
        })
        message.success('A new box created.');
    }
    boxremove = (boxkey) => {        
        let T = this.state.exchangeboxes;
        if (T.length == 1) {
            message.warning('Sorry. The last box cannot be removed.');
            return;
        }
        T.splice(boxkey, 1);
        let P = this.state.parities;
        P.splice(boxkey, 1);
        let key_array_3 = this.state.coin1keys;          key_array_3.splice(boxkey, 1);
        let key_array_4 = this.state.coin2keys;          key_array_4.splice(boxkey, 1);
        let key_array_7 = this.state.coin1keyinput;      key_array_7.splice(boxkey, 1);
        let key_array_8 = this.state.coin2keyinput;      key_array_8.splice(boxkey, 1);
        let key_array_11 = this.state.coin1keybutton;    key_array_11.splice(boxkey, 1);
        let key_array_12 = this.state.coin2keybutton;    key_array_12.splice(boxkey, 1);
        let key_array_13 = this.state.apikeypopover;     key_array_13.splice(boxkey, 1);
        let key_array_14 = this.state.secretkeypopover;  key_array_14.splice(boxkey, 1);
        let key_array_15 = this.state.coin1keypopover;   key_array_15.splice(boxkey, 1);
        let key_array_16 = this.state.coin2keypopover;   key_array_16.splice(boxkey, 1);

        this.setState({
            exchangeboxes: T,
            parities: P
        })
        message.success('The selected box removed.');
    }
    exchangeChange = (boxkey, value) => {
        let T = this.state.exchangeboxes;
        T[boxkey] = value;
        let P = this.state.parities;
        P[boxkey] = datacollection[value][0];
        this.setState({
            exchangeboxes: T,
            parities: P,
            selectdisble: true
        });
        switch (value) {
            case 'BINANCE':
                fetch('/binance-depth/' + datacollection[value][0].replace('/', ''))
                    .then(res => res.json())
                    .then(data => this.setState({
                        [value + datacollection[value][0].replace('/', '_')]: data
                    }))
                    .catch((error) => {})
                break;
            case 'BTCTURK':
                Promise.all([
                    fetch('https://www.btcturk.com/api/orderbook?pairSymbol=' + datacollection[value][0].replace('/', '')),
                    fetch('https://www.btcturk.com/api/trades?pairSymbol=' + datacollection[value][0].replace('/', '') + '&last=18')
                ])
                .then(([res1, res2]) => {
                    if (res1.status >= 400 && res1.status < 600 || res2.status >= 400 && res2.status < 600) {
                        return;
                    } else {
                        return Promise.all([res1.json(), res2.json()])
                    }
                }) 
                .then(([data1, data2]) =>{
                    if (data1 == null || data2 == null){
                        return;
                    } else {
                        this.setState({
                            [value + datacollection[value][0].replace('/', '_')]: data1, 
                            [value + datacollection[value][0].replace('/', '_') + 'history']: data2
                        })
                    }
                })
                .catch((error) => {})
                break;
            case 'KOINEKS':
                fetch('https://cors.io/?https://koineks.com/ticker')
                    .then(res => res.json())
                    .then(data => this.setState({
                        [value + datacollection[value][0].replace('/', '_')]: data
                    }))
                    .catch((error) => {})
                break;
        }
    }
    parityChange = (boxkey, value) => {
        let P = this.state.parities;
        P[boxkey] = value;
        this.setState({
            parities: P,
            selectdisble: true
        });
        switch (this.state.exchangeboxes[boxkey]) {
            case 'BINANCE':
                fetch('/binance-depth/' + value.replace('/', ''))
                    .then(res => res.json())
                    .then(data => this.setState({
                        [this.state.exchangeboxes[boxkey] + value.replace('/', '_')]: data
                    }))
                    .catch((error) => {})
                break;
            case 'BTCTURK':
                Promise.all([
                    fetch('https://www.btcturk.com/api/orderbook?pairSymbol=' + value.replace('/', '')),
                    fetch('https://www.btcturk.com/api/trades?pairSymbol=' + value.replace('/', '') + '&last=18')
                ])
                .then(([res1, res2]) => {
                    if (res1.status >= 400 && res1.status < 600 || res2.status >= 400 && res2.status < 600) {
                        return;
                    } else {
                        return Promise.all([res1.json(), res2.json()])
                    }
                })
                .then(([data1, data2]) => {
                    if (data1 == null || data2 == null){
                        return;
                    } else {
                        this.setState({
                            [this.state.exchangeboxes[boxkey] + value.replace('/', '_')]: data1,
                            [this.state.exchangeboxes[boxkey] + value.replace('/', '_') + 'history']: data2
                        })
                    }
                })
                .catch((error) => {})
                break;
            case 'KOINEKS':
                fetch('https://cors.io/?https://koineks.com/ticker')
                    .then(res => res.json())
                    .then(data => this.setState({
                        [this.state.exchangeboxes[boxkey] + value.replace('/', '_')]: data
                    }))
                    .catch((error) => {})
                break;
        }
    }
    warningClose = () => {
        this.stopAudio();
    }
    fiveInterval = () => {
        let array = [];
        for (let i=0; i<this.state.exchangeboxes.length; i++) {
            array.push({[this.state.exchangeboxes[i]]: this.state.parities[i]});
        }
        let uniquearray = [];
        for (let item of array) {
            let key = Object.keys(item)[0];
            let value = item[key];
            let isItem = true;
            for (let subitem of uniquearray) {
              if (Object.keys(subitem)[0] === key && subitem[Object.keys(subitem)[0]] === value) {
                isItem = false;
                break;
              }
            }
            if (isItem) {
                uniquearray.push({[key]:value});
            }
        }
        for (let item of uniquearray) {
            let key = Object.keys(item)[0];
            let value = item[key].replace('/', '');
            switch (key) {
                case 'BINANCE':
                    fetch('/binance-depth/' + value)
                        .then(res => res.json())
                        .then(data => this.setState({
                            ['BINANCE' + item[key].replace('/', '_')]: data
                        }))
                        .catch((error) => {})
                    break;
                case 'BTCTURK':
                    Promise.all([
                        fetch('https://www.btcturk.com/api/orderbook?pairSymbol=' + value),
                        fetch('https://www.btcturk.com/api/trades?pairSymbol=' + value + '&last=18')
                    ])
                    .then(([res1, res2]) => {
                        if (res1.status >= 400 && res1.status < 600 || res2.status >= 400 && res2.status < 600) {
                            return;
                        } else {
                            return Promise.all([res1.json(), res2.json()])
                        }
                    })
                    .then(([data1, data2]) => {
                        if (data1 == null || data2 == null){
                            return;
                        } else {
                            this.setState({
                                ['BTCTURK' + item[key].replace('/', '_')]: data1,
                                ['BTCTURK' + item[key].replace('/', '_') + 'history']: data2
                            })
                        }
                    })
                    .catch((error) => {})
                    break;
                case 'KOINEKS':
                    fetch('https://cors.io/?https://koineks.com/ticker')
                        .then(res => res.json())
                        .then(data => this.setState({
                            ['KOINEKS' + item[key].replace('/', '_')]: data
                        }))
                        .catch((error) => {})
                    break;
            }
        }
        // get all balance & price
        Promise.all([
            fetch('/balance-list'),
            fetch('/price-list')
        ])    
        .then(([res1, res2]) => {
            if (res1.status >= 400 && res1.status < 600 || res2.status >= 400 && res2.status < 600) {
                return;
            } else {
                return Promise.all([res1.json(), res2.json()])
            }
        })
        .then(([data1, data2]) => {
            if (data1 == null || data2 == null){
                return;
            } else {
                this.setState({
                    balancedata: {'BINANCE': data1.binance},
                    pricedata: {'BINANCE': data2.data}
                })
            }
        })
        .catch((error) => {})

        if (this.state.limitchangekey) {
            fetch('/paritylimit/' + this.state.limitchangeparam)
            this.setState({
                limitchangekey: false,
                limitchangeparam: ''
            })
        }
        let T = this.state.testtime + 1;
        this.setState({
            testtime: T
        })
        if (this.state.testtime > 120) {
            const key = 'warning';
            notification.info({
                key,
                message: 'Balance warning',
                description: 'Btcturk xrp/try has triggered alert at %1',
                icon: <Icon type="dollar" style={{ color: '#108ee9' }} />,
                duration: 0,
                style: {
                    paddingBottom: 30
                },
                onClose: this.warningClose
            });
            this.playAudio()
            this.setState({
                testtime: 0
            })
        }
    }
    popKeyEnter = (e, boxkey, buttonkey) => {  
        if (e.keyCode === 13 && e.target.value) {
            let T = {
                'BINANCE' : 1,
                'BTCTURK' : 2,
                'KOINEKS' : 3
            }
            if (buttonkey == 1 || buttonkey == 2)
                fetch('/accountkey/' + T[this.state.exchangeboxes[boxkey]] + '/' + buttonkey + '/' + e.target.value)
            else
                fetch('/coinkey/' + T[this.state.exchangeboxes[boxkey]] + '/' + buttonkey + '/' + e.target.value)
            var P, Q, R;
            switch (buttonkey) {
                case 1:
                    T = this.state.apikeys;       T[this.state.exchangeboxes[boxkey]] = e.target.value;
                    R = this.state.apikeypopover; R[boxkey] = false;
                    this.setState({
                        apikeys: T,
                        apikeypopover: R
                    })
                    break;
                case 2:
                    T = this.state.secretkeys;       T[this.state.exchangeboxes[boxkey]] = e.target.value;
                    R = this.state.secretkeypopover; R[boxkey] = false;
                    this.setState({
                        secretkeys: T,
                        secretkeypopover: R
                    })
                    break;
                case 3:
                    T = this.state.coin1keys;       T[boxkey] = e.target.value;
                    P = this.state.coin1keyinput;   P[boxkey] = 'block';
                    Q = this.state.coin1keybutton;  Q[boxkey] = 'none';
                    R = this.state.coin1keypopover; R[boxkey] = false;
                    this.setState({
                        coin1keys: T,
                        coin1keyinput: P,
                        coin1keybutton: Q,
                        coin1keypopover: R
                    })
                    break;
                case 4:
                    T = this.state.coin2keys;       T[boxkey] = e.target.value;
                    P = this.state.coin2keyinput;   P[boxkey] = 'block';
                    Q = this.state.coin2keybutton;  Q[boxkey] = 'none';
                    R = this.state.coin2keypopover; R[boxkey] = false;
                    this.setState({
                        coin2keys: T,
                        coin2keyinput: P,
                        coin2keybutton: Q,
                        coin2keypopover: R
                    })
                    break;
            }            
        }
    }
    handleVisibleChange = (boxkey, buttonkey) => {
        let T;
        switch (buttonkey) {
            case 1:
                T = this.state.apikeypopover;
                T[boxkey] = !T[boxkey];
                this.setState({
                    apikeypopover: T
                });
                break;
            case 2:
                T = this.state.secretkeypopover;
                T[boxkey] = !T[boxkey];
                this.setState({
                    secretkeypopover: T
                });
                break;
            case 3:
                T = this.state.coin1keypopover;
                T[boxkey] = !T[boxkey];
                this.setState({
                    coin1keypopover: T
                });
                break;
            case 4:
                T = this.state.coin2keypopover;
                T[boxkey] = !T[boxkey];
                this.setState({
                    coin2keypopover: T
                });
                break;
        }
    }
    limitchange = (value, exchange, parity, type) => {
        let T = this.state.paritylimits;
        if (T[exchange][parity] == undefined) T[exchange][parity] = [];
        T[exchange][parity][type] = value;
        this.setState({
            paritylimits: T,
            
            limitchangekey: true,
            limitchangeparam: exchange + '/' + parity + '/' + value + '/' + type
        })
        this.stopAudio();
        notification.close('warning');
    }
    playAudio = () => {
        this.sound.play();
    }
    stopAudio = () => {
        this.sound.pause();
        this.sound.currentTime = 0;
        this.setState({
            testtime: 0
        })
    }

    componentDidMount = () => {
        Promise.all([
            fetch('/binance-depth/' + datacollection['BINANCE'][0].replace('/', '')),
            fetch('https://www.btcturk.com/api/orderbook?pairSymbol=' + datacollection['BTCTURK'][0].replace('/', '')),
            fetch('https://www.btcturk.com/api/trades?pairSymbol=' + datacollection['BTCTURK'][0].replace('/', '') + '&last=18'),
            fetch('https://cors.io/?https://koineks.com/ticker'),
            
            fetch('/getaccountkey'),
            fetch('/getparitylimit'),
            fetch('/balance-list')
        ])
        .then(([res1, res2, res3, res4, res5, res6]) => Promise.all([res1.json(), res2.json(), res3.json(), res4.json(), res5.json(), res6.json()]))
        .then(([data1, data2, data3, data4, data5, data6]) => {
            let limits = this.state.paritylimits;
            for (let record of data6.paritylimits) {
                let T = [];
                T[0] = record.blimit;
                T[1] = record.rlimit;
                switch (record.exchange) {
                    case 1:
                        limits['BINANCE'][record.parity] = T;
                        break;
                    case 2:
                        limits['BTCTURK'][record.parity] = T;
                        break;
                    case 3:
                        limits['KOINEKS'][record.parity] = T;
                        break;
                }
            }
            
            this.setState({
                ['BINANCE' + datacollection['BINANCE'][0].replace('/', '_')]: data1, 
                ['BTCTURK' + datacollection['BTCTURK'][0].replace('/', '_')]: data2,
                ['BTCTURK' + datacollection['BTCTURK'][0].replace('/', '_') + 'history']: data3,
                ['KOINEKS' + datacollection['KOINEKS'][0].replace('/', '_')]: data4,
                apikeys: data5.apikeys,
                secretkeys: data5.secretkeys,
                paritylimits: limits,
                // balancedata: {'BINANCE': data7.binance}
            })
        })
        .catch((error) => {})
        
        this.timer = setInterval(() => this.fiveInterval(), 5000)
    }
    componentDidUpdate = () => {
        if (this.state.selectdisble)
            this.setState({
                selectdisble: false,
            });
    }
    componentWillUnmount = () => {
        clearInterval(this.timer);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////                PAGE RENDER                  ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    render() {
        return (                
            <div>
                <Row>
                    <Menubar current='exchange'/>
                    <audio ref={(sound) => {this.sound = sound}} loop>
                        <source src="/static/mp3/2.mp3" type="audio/mpeg" />
                    </audio>
                </Row>
                <Row gutter={6}>
                    {
                        this.state.exchangeboxes.map((box, i) => (
                            <Exchangebox                                
                                key={i}
                                // value props
                                boxkey              = {i}
                                exchange            = {box}
                                parity              = {this.state.parities[i]}
                                EPstate             = {this.state[box + this.state.parities[i].replace('/', '_')]}
                                EPsh                = {this.state[box + this.state.parities[i].replace('/', '_') + 'history']}
                                
                                apikeys             = {this.state.apikeys}
                                secretkeys          = {this.state.secretkeys}
                                paritylimits        = {this.state.paritylimits}

                                coin1keys           = {this.state.coin1keys}
                                coin2keys           = {this.state.coin2keys}
                                coin1keyinput       = {this.state.coin1keyinput}
                                coin2keyinput       = {this.state.coin2keyinput}
                                coin1keybutton      = {this.state.coin1keybutton}
                                coin2keybutton      = {this.state.coin2keybutton}

                                apikeypopover       = {this.state.apikeypopover}
                                secretkeypopover    = {this.state.secretkeypopover}
                                coin1keypopover     = {this.state.coin1keypopover}
                                coin2keypopover     = {this.state.coin2keypopover}
                                
                                balancedata         = {this.state.balancedata}
                                pricedata           = {this.state.pricedata}

                                // action props
                                boxadd              = {this.boxadd}
                                boxremove           = {this.boxremove}
                                parityChange        = {this.parityChange}
                                exchangeChange      = {this.exchangeChange}

                                popKeyEnter         = {this.popKeyEnter}
                                handleVisibleChange = {this.handleVisibleChange}
                                limitchange  = {this.limitchange}
                                playAudio           = {this.playAudio}
                            />
                        ))
                    }
                </Row>
            </div>
          )
      }
  }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////                PIE CHART                  ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getAnglePoint(startAngle, endAngle, radius, x, y) {
	var x1, y1, x2, y2;

	x1 = x + radius * Math.cos(Math.PI * startAngle / 180);
	y1 = y + radius * Math.sin(Math.PI * startAngle / 180);
	x2 = x + radius * Math.cos(Math.PI * endAngle / 180);
	y2 = y + radius * Math.sin(Math.PI * endAngle / 180);

	return { x1, y1, x2, y2 };
}

const Pie = (props) => {	
    var colors = props.colors,
        colorsLength = colors.length,
        labels = props.labels,
        hole = props.hole,
        radius = props.radius,
        diameter = radius * 2,
        sum, startAngle = null;
		sum = props.data.reduce(function (carry, current) { return carry + current }, 0);
        startAngle = 0;
		return (
			<svg width={ diameter } height={ diameter } viewBox={ '0 0 ' + diameter + ' ' + diameter } version="1.1">
				{ props.data.map(function (slice, sliceIndex) {
					var angle, nextAngle, percent;
					nextAngle = startAngle;
					angle = (slice / sum) * 360;
					percent = (slice / sum) * 100;
                    startAngle += angle;
					return <Slice
                        key={ sliceIndex }
                        key1={ sliceIndex }
						value={ slice }
						percent={ props.percent }
						percentValue={ percent.toFixed(1) }
						startAngle={ nextAngle }
						angle={ angle }
						radius={ radius }
						hole={ radius - hole }
						trueHole={ hole }
						showLabel= { labels }
						fill={ colors[sliceIndex % colorsLength] }
						stroke={ props.stroke }
						strokeWidth={ props.strokeWidth }
                    />
				}) }
			</svg>
		);
}
class Slice extends React.Component {
	state = {
        path: '',
        x: 0,
        y: 0
	}
	componentWillReceiveProps = () => {
		this.setState({ path: '' });
		this.animate();
	}
	componentDidMount = () => {
		this.animate();
	}
	animate = () => {
		this.draw(0);
	}
	draw = (s) => {
		var p = this.props, path = [], a, b, c, self = this, step;
		step = p.angle / (37.5 / 2);
		if (s + step > p.angle) {
			s = p.angle;
		}
		a = getAnglePoint(p.startAngle, p.startAngle + s, p.radius, p.radius, p.radius);
        b = getAnglePoint(p.startAngle, p.startAngle + s, p.radius - p.hole, p.radius, p.radius);        
		path.push('M' + a.x1 + ',' + a.y1);
		path.push('A'+ p.radius +','+ p.radius +' 0 '+ (s > 180 ? 1 : 0) +',1 '+ a.x2 + ',' + a.y2);
		path.push('L' + b.x2 + ',' + b.y2);
		path.push('A'+ (p.radius- p.hole) +','+ (p.radius- p.hole) +' 0 '+ (s > 180 ? 1 : 0) +',0 '+ b.x1 + ',' + b.y1);
		path.push('Z');
		this.setState({ path: path.join(' ') });
		if (s < p.angle) {
            self.draw(s + step);
		} else if (p.showLabel) {
			c = getAnglePoint(p.startAngle, p.startAngle + (p.angle / 2), (p.radius / 2 + p.trueHole / 2), p.radius, p.radius);
			this.setState({
				x: c.x2,
				y: c.y2
			});
		}
	}
	render() {
		return (
			<g overflow="hidden">
				<path
					d={ this.state.path }
					fill={ this.props.fill }
					stroke={ this.props.stroke }
					strokeWidth={ this.props.strokeWidth ? this.props.strokeWidth : 3 }
				/>
				{ this.props.showLabel && this.props.percentValue > 5 ?
					<text x={ this.state.x } y={ this.state.y } fill="#fff" textAnchor="middle">
                        {/* { this.props.key1 == 1 ? this.props.value + '$' : 'BTC' } */}
                        {this.props.value}
					</text>
				: null }
			</g>
		);
	}
}